; -*- lisp -*-
(require 'package)
(package-initialize)
(add-to-list 'load-path (expand-file-name "~/.emacs.d/packages"))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(abbrev-mode nil t)
 '(ack-and-a-half-executable "/usr/local/bin/ack")
 '(ack-and-a-half-use-ido t)
 '(ag-reuse-buffers t)
 '(auto-mode-case-fold t)
 '(auto-save-default nil)
 '(auto-save-visited-file-name nil)
 '(blink-cursor-mode nil)
 '(browse-url-browser-function (quote browse-url-default-browser))
 '(browse-url-firefox-program "firefox-3.5")
 '(case-fold-search t)
 '(coffee-args-compile (quote ("-m" "-c")))
 '(coffee-command
   "/usr/local/share/npm/lib/node_modules/coffee-script/bin/coffee")
 '(coffee-tab-width 2)
 '(column-number-mode t)
 '(css-indent-offset 2)
 '(current-language-environment "English")
 '(cursor-in-non-selected-windows nil)
 '(custom-enabled-themes (quote (solarized-dark)))
 '(custom-safe-themes
   (quote
    ("8aebf25556399b58091e533e455dd50a6a9cba958cc4ebb0aab175863c25b9a4" "1e7e097ec8cb1f8c3a912d7e1e0331caeed49fef6cff220be63bd2a6ba4cc365" "fc5fcb6f1f1c1bc01305694c59a1a861b008c534cae8d0e48e4d5e81ad718bc6" "71b172ea4aad108801421cc5251edb6c792f3adbaecfa1c52e94e3d99634dee7" "e9cd13d22eab8a441c0c7d319dc044881723395b4aa22f283d581d921cc94a87" "9cdf9fb94f560902b567b73f65c2ed4e5cfbaafe" default)))
 '(desktop-clear-preserve-buffers
   (quote
    ("\\*scratch\\*" "\\*Messages\\*" "\\*server\\*" "\\*tramp/.+\\*" "\\*Warnings\\*" "\\*magit: .+\\*")))
 '(desktop-file-name-format (quote tilde))
 '(desktop-lazy-idle-delay 10)
 '(desktop-lazy-verbose nil)
 '(desktop-path (quote ("~/.emacs.d/")))
 '(desktop-restore-eager 5)
 '(ff-case-fold-search t)
 '(fill-column 77)
 '(flycheck-display-errors-delay 0.4)
 '(flycheck-flake8rc "~/.config/flake8")
 '(flycheck-highlighting-mode (quote lines))
 '(flycheck-jshintrc ".jshintrc")
 '(flycheck-mode-hook
   (quote
    (flycheck-mode-set-explicitly flycheck-color-mode-line-mode)))
 '(global-flycheck-mode t nil (flycheck))
 '(global-font-lock-mode t nil (font-lock))
 '(global-visual-line-mode nil)
 '(global-whitespace-mode nil)
 '(grep-find-template "find . <X> -type f <F> -exec grep <C> -nH -e <R> '{}' + ")
 '(gud-gdb-command-name "gdb --annotate=1")
 '(haskell-check-command (expand-file-name "~/.cabal/bin/hlint"))
 '(haskell-mode-hook (quote (turn-on-haskell-indentation turn-on-font-lock)))
 '(icomplete-mode t)
 '(ido-enable-flex-matching t)
 '(ido-everywhere t)
 '(ido-mode t nil (ido))
 '(indent-tabs-mode nil)
 '(inhibit-eol-conversion t)
 '(inhibit-startup-screen t)
 '(initial-buffer-choice t)
 '(initial-frame-alist (quote ((fullscreen . maximized))))
 '(ispell-alternate-dictionary "/usr/share/dict/words")
 '(ispell-complete-word-dict "/usr/share/dict/words")
 '(js-indent-level 2)
 '(js2-basic-offset 2)
 '(js2-global-externs (quote ("define")))
 '(js2-mode-show-parse-errors nil)
 '(js2coffee-command "/usr/local/bin/js2coffee")
 '(large-file-warning-threshold nil)
 '(line-move-visual t)
 '(lineker-check-on-save nil)
 '(lineker-column-limit 80)
 '(lineker-warning-beep nil)
 '(magit-default-tracking-name-function (quote magit-default-tracking-name-branch-only))
 '(magit-repo-dirs (list (expand-file-name "~/Projects")))
 '(magit-rewrite-inclusive (quote ask))
 '(magit-save-some-buffers nil)
 '(magit-server-window-for-commit nil)
 '(magit-server-window-for-rebase nil)
 '(markdown-indent-on-enter nil)
 '(mf-display-padding-height 0)
 '(mf-display-padding-width 0)
 '(mf-max-height nil)
 '(mouse-autoselect-window t)
 '(mouse-avoidance-mode (quote jump) nil (avoid))
 '(mouse-wheel-mode t nil (mwheel))
 '(mumamo-check-chunk-major-same nil)
 '(mumamo-chunk-coloring (quote submode-colored))
 '(mumamo-major-modes
   (quote
    ((php-mode php-mode)
     (css-mode css-mode)
     (java-mode java-mode)
     (ruby-mode ruby-mode)
     (django-mode django-nxhtml-mode)
     (perl-mode perl-mode)
     (asp-js-mode javascript-mode)
     (asp-vb-mode visual-basic-mode)
     (javascript-mode javascript-mode ecmascript-mode))))
 '(mumamo-set-major-mode-delay -1)
 '(mumamo-submode-indent-offset 4)
 '(nxhtml-auto-mode-alist
   (quote
    (("\\.x?html?\\'" . nxhtml-mumamo)
     ("\\.x?htmlf?\\'" . nxhtml-mumamo)
     ("\\.php\\'" . nxhtml-mumamo)
     ("\\.phtml\\'" . nxhtml-mumamo)
     ("\\.jsp\\'" . jsp-nxhtml-mumamo)
     ("\\.asp\\'" . asp-nxhtml-mumamo)
     ("\\.djhtml\\'" . django-nxhtml-mumamo)
     ("\\.rhtml\\'" . eruby-nxhtml-mumamo)
     ("\\.phps\\'" . smarty-nxhtml-mumamo)
     ("\\.epl\\'" . embperl-nxhtml-mumamo)
     (".lzx\\'" . laszlo-nxml-mumamo)
     ("\\.js\\'" . javascript-mode)
     ("\\.css\\'" . css-mode)
     ("templates/.*/.*\\.html\\'" . django-nxhtml-mumamo))))
 '(nxhtml-minor-mode-modes
   (quote
    (nxhtml-mode nxml-mode html-mode sgml-mode xml-mode php-mode css-mode javascript-mode java-mode image-mode dired-mode)))
 '(nxhtml-skip-welcome t)
 '(package-archives
   (quote
    (("gnu" . "http://elpa.gnu.org/packages/")
     ("marmalade" . "http://marmalade-repo.org/packages/")
     ("melpa" . "http://melpa.milkbox.net/packages/"))))
 '(python-check-command "/usr/local/bin/flake8")
 '(python-fill-docstring-style (quote django))
 '(python-guess-indent nil)
 '(python-indent-guess-indent-offset nil)
 '(python-indent-offset 4)
 '(python-skeleton-autoinsert t)
 '(python-use-skeletons t)
 '(read-file-name-completion-ignore-case t)
 '(ruby-align-to-stmt-keywords (quote (def)))
 '(ruby-deep-arglist nil)
 '(ruby-deep-indent-paren nil)
 '(ruby-insert-encoding-magic-comment nil)
 '(safe-local-variable-values (quote ((encoding . utf-8))))
 '(savehist-file "~/.emacs-history")
 '(scroll-bar-mode nil)
 '(scss-sass-command (quote ~/\.rvm/gems/ruby-1\.9\.3-p448/bin/sass))
 '(server-mode t)
 '(server-window nil)
 '(show-paren-mode t)
 '(show-trailing-whitespace t)
 '(simple-rtm-sort-order (quote priority-date-name))
 '(standard-indent 4)
 '(tool-bar-mode nil)
 '(tramp-auto-save-directory "/tmp")
 '(tramp-default-method "sudo")
 '(transient-mark-mode t)
 '(twisted-dev-directory "~/Projects/Twisted")
 '(twisted-dev-scratch-directory "/tmp")
 '(uniquify-buffer-name-style (quote post-forward) nil (uniquify))
 '(vc-follow-symlinks t)
 '(visible-bell t)
 '(window-number-meta-mode t)
 '(window-number-mode t))

(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(default ((t (:slant normal :weight light :height 120 :width normal :foundry "apple" :family "Source_Code_Pro")))))
(add-to-list 'custom-theme-load-path "~/.emacs.d/packages/emacs-color-theme-solarized-master")
;; add directories inside .emacs.d to the path
;; (mapc #'(lambda (dir)
;;           (add-to-list 'load-path (concat "~/.emacs.d/" dir)))
;;           (cddr (directory-files "~/.emacs.d")))
(add-to-list 'load-path (expand-file-name "~/.emacs.d/packages"))
(add-to-list 'load-path "/usr/share/emacs/site-lisp")
(mapc #'(lambda (dir)
          (add-to-list 'load-path (concat "/usr/share/emacs/site-lisp/" dir)))
          (cddr (directory-files "/usr/share/emacs/site-lisp")))
(require 'uniquify)
(require 'tramp)
;(require 'maxframe)
;(require 'ecb)
;(require 'twit)
(require 'flyspell)
(require 'window-number "window-number" t)

;; Hide the menu bar on non-Mac systems.  On OS X, this just leaves a blank
;; space where the menu bar is, and that looks pretty strange.
(if (not (eq system-type 'darwin))
    (custom-set-variables
     '(menu-bar-mode nil)
     '(ns-auto-hide-menu-bar t)))

(put 'test-case-name 'safe-local-variable 'identity) ; disable the dumb test-case-name warning

(setenv "PATH" "/usr/local/bin:/usr/local/share/npm/bin:/usr/bin:/bin/:/usr/sbin:/sbin:/usr/X11R6/bin:~/.rvm/bin")
(setenv "TNS_ADMIN" "/etc")
(add-to-list 'exec-path "/usr/local/bin")
(add-to-list 'exec-path "/usr/local/share/npm/bin")
(add-to-list 'exec-path (expand-file-name "~/.rvm/bin"))

(add-to-list 'auto-mode-alist
             '("tumblr-post" . tumblr-mode))
(global-set-key "\r" 'newline-and-indent)
(global-set-key "\C-xg" 'magit-status)
(global-set-key "\C-cgb" 'magit-blame-mode)
(global-set-key "\C-cts" 'tumblr-start-post)
(global-set-key "\C-ctp" 'tumblr-post)
(global-set-key "\C-ce" 'eshell)
(global-set-key "\C-xr\r" 'rgrep)
;; (global-set-key "\C-xa\r" 'ack-and-a-half)
(global-set-key "\C-xas" 'ack-and-a-half-same)
(global-set-key "\C-xa\r" 'ag-project)

(put 'downcase-region 'disabled nil)
(put 'upcase-region 'disabled nil)
(make-variable-buffer-local 'tumblr-email)
(make-variable-buffer-local 'tumblr-password)
(global-set-key "\C-cfs" 'flycheck-buffer)
(global-set-key "\C-cfn" 'flycheck-next-error)
(global-set-key "\C-cfp" 'flycheck-previous-error)
(global-set-key (kbd "s-x") 'execute-extended-command)
(global-set-key (kbd "s-/") 'dabbrev-expand)
(global-set-key (kbd "s-q") 'fill-paragraph)

(defun buffer-file-name-line ()
  (format "%s:%d"
          (buffer-file-name)
          (line-number-at-pos)))
(global-set-key (kbd "C-c C-l")
                '(lambda ()
                   (interactive)
                   (kill-new (buffer-file-name-line))))


(add-hook 'js2-mode-hook 'fci-mode)
(add-hook 'coffee-mode-hook 'fci-mode)

(add-hook 'ruby-mode-hook 'fci-mode)
(defun directory-with-spec (base)
  "Find a parent directory of the given base with a spec/ directory"
  (if (file-directory-p (concat base "spec"))
      base
    (directory-with-spec (expand-file-name (concat base "../")))))
(defun rspec (&optional current_file)
  "Run rspec spec/ for the current project"
  (interactive "P")
  (let* ((spec-root (directory-with-spec (expand-file-name default-directory)))
         (spec-root-base (file-name-base (substring spec-root 0 -1)))
         (rspec-buffer-name (format "*rspec: %s*" spec-root-base))
         (buffer (current-buffer))
         (spec-path (if current_file  ; use the current file/line
                        (buffer-file-name-line)
                      ""))
         (rspec-command (format "%s %s --no-color"
                                (expand-file-name "~/bin/rspec-wrapper")
                                spec-path))
         (compilation-buffer-name-function (lambda (mode) rspec-buffer-name)))
    (when (get-buffer rspec-buffer-name)
      (ido-visit-buffer rspec-buffer-name 'raise-frame))
    (cd spec-root)
    (compile rspec-command)
    (ido-visit-buffer buffer 'raise-frame)))
(defun rspec-recompile ()
  "Recompile the current project"
  (interactive)
  (let* ((spec-root (directory-with-spec (expand-file-name default-directory)))
         (spec-root-base (file-name-base (substring spec-root 0 -1)))
         (rspec-buffer-name (format "*rspec: %s*" spec-root-base))
         (buffer (current-buffer)))
    (ido-visit-buffer rspec-buffer-name 'raise-frame)
    (recompile)
    (ido-visit-buffer buffer 'raise-frame)))
(defun rspec-set-key ()
  "Set C-b C-e C-r as a local key to run the RSpec tests"
  (local-set-key (kbd "C-b C-e C-r")
                 'rspec)
  (local-set-key (kbd "<f13>")
                 'rspec)
  (local-set-key (kbd "C-b C-e C-c")
                 'rspec-recompile)
  (local-set-key (kbd "<f14>")
                 'rspec-recompile))
(add-hook 'ruby-mode-hook 'rspec-set-key)
(add-hook 'magit-mode-hook 'rspec-set-key)
(add-hook 'compilation-mode 'rspec-set-key)

(setq magit-completing-read-function
    'magit-ido-completing-read)

(defun delete-trailing-whitespace-on-save ()
  "Delete trailing whitespace on save"
  (add-to-list 'write-file-functions 'delete-trailing-whitespace))
(add-hook 'python-mode-hook 'delete-trailing-whitespace-on-save)
(add-hook 'ruby-mode-hook 'delete-trailing-whitespace-on-save)
(add-hook 'js2-mode-hook 'delete-trailing-whitespace-on-save)
(add-hook 'scss-mode-hook 'delete-trailing-whitespace-on-save)

(when (executable-find "terminal-notifier")
  (defun terminal-notifier-build-finished (buffer msg)
    (let* ((formatted-subtitle (if (string-match "^finished" msg)
                                   "Successful"
                                 "Failed")))
      (start-process "build-notification" buffer
                     "terminal-notifier" "-message" msg
                     "-group" (buffer-name buffer) "-title" (buffer-name buffer)
                     "-subtitle" formatted-subtitle
                     "-activate" "org.gnu.Emacs" "-sender" "org.gnu.Emacs")))
  (add-hook 'compilation-finish-functions 'terminal-notifier-build-finished)
  )

; use file-truename when finding the name of the directory
(defun gud-format-command (str arg)
  (let ((insource (not (eq (current-buffer) gud-comint-buffer)))
	(frame (or gud-last-frame gud-last-last-frame))
	result)
    (while (and str
		(let ((case-fold-search nil))
		  (string-match "\\([^%]*\\)%\\([adefFlpc]\\)" str)))
      (let ((key (string-to-char (match-string 2 str)))
	    subst)
	(cond
	 ((eq key ?f)
	  (setq subst (file-name-nondirectory (if insource
						  (buffer-file-name)
						(car frame)))))
	 ((eq key ?F)
	  (setq subst (file-name-sans-extension
		       (file-name-nondirectory (if insource
						   (buffer-file-name)
						 (car frame))))))
	 ((eq key ?d)
	  (setq subst (file-truename (file-name-directory (if insource
					       (buffer-file-name)
					     (car frame))))))
	 ((eq key ?l)
	  (setq subst (int-to-string
		       (if insource
			   (save-restriction
			     (widen)
			     (+ (count-lines (point-min) (point))
				(if (bolp) 1 0)))
			 (cdr frame)))))
	 ((eq key ?e)
	  (setq subst (gud-find-expr)))
	 ((eq key ?a)
	  (setq subst (gud-read-address)))
	 ((eq key ?c)
	  (setq subst
                (gud-find-class
                 (if insource
                      (buffer-file-name)
                    (car frame))
                 (if insource
                      (save-restriction
                        (widen)
                        (+ (count-lines (point-min) (point))
                           (if (bolp) 1 0)))
                    (cdr frame)))))
	 ((eq key ?p)
	  (setq subst (if arg (int-to-string arg)))))
	(setq result (concat result (match-string 1 str) subst)))
      (setq str (substring str (match-end 2))))
    ;; There might be text left in STR when the loop ends.
    (concat result str)))

; send PDB the full path
(defun pdb-custom-setup ()
  (gud-def gud-break  "break %d%f:%l"     "\C-b" "Set breakpoint at current line.")
  (gud-def gud-remove "clear %d%f:%l"     "\C-d" "Remove breakpoint at current line"))

(add-hook 'pdb-mode-hook 'pdb-custom-setup)

(add-to-list 'auto-mode-alist
             '("\\.pp\\'" . puppet-mode))
(add-to-list 'auto-mode-alist
             '("\\.yaml\\'" . yaml-mode))
(add-to-list 'auto-mode-alist
             '("\\.mustache\\'" . mustache-mode))
(add-to-list 'auto-mode-alist
             '("\\.js\\'" . js2-mode))
(add-to-list 'auto-mode-alist
             '("\\.scss\\'" . sass-mode))
(add-to-list 'auto-mode-alist
             '("\\.geojson\\'" . json-mode))
(add-to-list 'auto-mode-alist
             '("\\.jsx\\'" . web-mode))

(defalias 'yes-or-no-p 'y-or-n-p)

; Search with Dash (http://kapeli.com/)
(autoload 'dash-at-point "dash-at-point"
  "Search the word at point with Dash." t nil)
(global-set-key "\C-cd" 'dash-at-point)

(global-set-key "\C-c\C-f" 'json-pretty-print-buffer)

; enable windmove
(windmove-default-keybindings)

(defun underscore ()
  (interactive)
  (replace-regexp "\\([A-Z]\\)" "_\\1" nil (region-beginning)(region-end))
  (downcase-region (region-beginning)(region-end)))
(defun camelcase ()
  (interactive)
  (replace-regexp "_\\([a-z]\\)" "\\,(upcase \\1)" nil (region-beginning)(region-end)))

; Make this last, so that it overrides other faces
(load-theme 'solarized-dark t)

(provide 'emacs)
;;; emacs ends here
