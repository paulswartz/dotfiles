#!/bin/sh

# Sets up the local environment with links to the files in this repository.

here="$( cd "$(dirname "$0")" ; pwd -P )"

_ln() {
  rm -r $2
  ln -Ffhs $here/$1 $2
}

for dotfile in emacs gitconfig gitignore_global tmux.conf slate ackrc jshintrc agignore; do
    _ln $dotfile ~/.${dotfile}
done

# scripts
_ln bin ~/bin

mkdir -p ~/.emacs.d
_ln packages/emacs ~/.emacs.d/packages

mkdir -p ~/.pip/cache
_ln pip.conf ~/.pip/pip.conf

# Fish
mkdir -p ~/.config/fish
for c in config.fish fish_prompt.fish fish_right_prompt.fish git-alias.fish ssh-agent.fish virtual.fish bundle.fish functions; do
    _ln fish/$c ~/.config/fish/$c
done

_ln flake8 ~/.config/flake8

# OS X
if [ -x /usr/bin/sw_vers ]; then
    . $here/packages/brew.sh
fi

# Python
. $here/packages/python.sh

# Ruby
. $here/packages/ruby.sh
