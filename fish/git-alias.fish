# Lazy git aliases
alias g git
alias gc "g commit"
alias gco "g checkout"
alias gs "g status"
alias gl "g lg"
alias gd "g diff"
alias gbs "g bisect start"
alias gbg "g bisect good"
alias gbb "g bisect bad"
alias amend "git commit --amend --no-edit"