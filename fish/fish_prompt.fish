
# Display the following bits on the left:
# * Virtualenv name (if applicable, see https://github.com/adambrenecki/virtualfish)
# * Current directory name
# * Git branch and dirty state (if inside a git repo)

function _git_branch_name
  echo (git symbolic-ref HEAD ^/dev/null | sed -e 's|^refs/heads/||')
end

function _is_git_dirty
  echo (git status -s --ignore-submodules=dirty ^/dev/null)
end

function fish_prompt
  set -l cyan (set_color cyan)
  set -l yellow (set_color yellow)
  set -l red (set_color red)
  set -l blue (set_color blue)
  set -l green (set_color green)
  set -l normal (set_color normal)

  set -l cwd $cyan(basename (prompt_pwd))

  # output the prompt, left to right

  # Display [venvname] if in a virtualenv
  if set -q VIRTUAL_ENV
      echo -n -s (set_color -b cyan black) '[' (basename "$VIRTUAL_ENV") ']' $normal
  end

  # Display the current directory name
  echo -n -s $cwd $normal


  # Show git branch and dirty state
  if [ (_git_branch_name) ]
    set -l git_branch '(' (_git_branch_name) ')'
    if [ (_is_git_dirty) ]
      set git_info $red $git_branch
    else
      set git_info $green $git_branch
    end
    echo -n -s '·' $git_info $normal  # unicode
    #e. cho -n -s '*' $git_info $normal  # non-unicode
  end

  # Terminate with a nice prompt char
  #echo -n -s '⟩ ' $normal  # boring arrow
  #echo -n -s '🍔  ' $normal  # hamburger
  echo -n -s '🍺  ' $normal  # beer
  #echo -n -s '$ ' $normal  # boring non-unicode

  # add current directory to z
  z --add "$PWD"
end