# Remove the Fish greeting
set fish_greeting

. ~/.config/fish/git-alias.fish
. ~/.config/fish/ssh-agent.fish
. ~/.config/fish/bundle.fish
if [ -r ~/.config/fish/local.fish ]
   . ~/.config/fish/local.fish
end

# initialize Combinator
set -x COMBINATOR_PATHS ~/Projects/combinator_paths
set -x COMBINATOR_PROJECTS ~/Projects
set -x PATH ~/Projects/combinator_paths/bincache ~/Projects/Divmod/Combinator/bin ~/bin /usr/local/bin /usr/local/sbin ~/Library/Python/2.7/bin ~/.cabal/bin /usr/local/share/npm/bin $PATH
set -x PYTHONPATH ~/Projects/Divmod/Combinator $PYTHONPATH

# jRuby Options
set -x JRUBY_OPTS '--1.9 -J-Xmx1024m -J-XX:MaxPermSize=256m -Xcompile.invokedynamic=false -J-XX:+TieredCompilation -J-XX:TieredStopAtLevel=1 -J-noverify -Xcompile.mode=OFF --headless -J-XX:+CMSClassUnloadingEnabled -J-XX:+UseConcMarkSweepGC'

# Try to use emacsclient, but fall back to Nano
set -x EDITOR /Applications/Emacs.app/Contents/MacOS/bin/emacsclient
if [ ! -x "$EDITOR" ]
   set -x EDITOR emacsclient
end
set -x ALTERNATE_EDITOR nano
set -x JAVA_HOME (/usr/libexec/java_home -v 1.6)
set -x AWS_IAM_HOME "/usr/local/Cellar/aws-iam-tools/1.5.0/jars"
set -x AWS_AUTO_SCALING_HOME "/usr/local/Library/LinkedKegs/auto-scaling/jars"
set -x EC2_AMITOOL_HOME "/usr/local/Library/LinkedKegs/ec2-ami-tools/jars"
set -x AWS_ELB_HOME "/usr/local/Library/LinkedKegs/elb-tools/jars"
set -x EC2_HOME "/usr/local/Library/LinkedKegs/ec2-api-tools/jars"
set -x WORKON_HOME ~/Projects/
set -x VIRTUALFISH_HOME ~/Projects/
#set -x DOCKER_HOST tcp://localhost:4243
if which docker-osx > /dev/null
   eval (docker-osx env | sed -e 's/export/set -x/' -e 's/=/ /')
end
# has to come after setting VIRTUALFISH_HOME
. ~/.config/fish/virtual.fish

if [ -f ~/.rvm/scripts/rvm ]
   rvm info > /dev/null # initialize RVM
end

# Always use sudo for apt-get since I'm lazy
function apt-get
    sudo apt-get $argv
end

# Make iTerm jump after a command is done: COMMAND; beep
function beep
    printf '\a'
end


. ~/.config/fish/fish_prompt.fish
. ~/.config/fish/fish_right_prompt.fish
