function cd --description "Change working directory"
  if test -z "$argv"
    builtin cd
    return
  end

  builtin cd "$argv"

  # activate a virtualenv environment
  if test -f virtualenv/bin/activate.fish
    sourcevirtualenv virtualenv
  end

  if test -f .ruby-version; or test -f .rvmrc
    rvm reload 2>&1 > /dev/null
  end

  return 0
end

function sourcevirtualenv --description "Activate a virtualenv, given the full path to the virtualenv"
	# check arguments
	if [ (count $argv) -lt 1 ]
		echo "You need to specify a virtualenv."
		return 1
	end

        set -l cwd $PWD
        builtin cd "$argv[1]"
        set -l full_path $PWD
        builtin cd "$cwd"
        set -e cwd

	if not [ -d $full_path ]
		echo "The virtualenv $argv[1] does not exist."
		echo "You can create it with mkvirtualenv."
		return 2
	end

	#Check if a different env is being used
	if set -q VIRTUAL_ENV
		devirtualenv
	end


        set -l base_path (dirname $full_path)

	emit virtualenv_will_activate
	emit virtualenv_will_activate:$full_path

	set -gx VIRTUAL_ENV (basename $base_path)
	set -g _VF_EXTRA_PATH $full_path/bin
	set -gx PATH $_VF_EXTRA_PATH $PATH

	# hide PYTHONHOME
	if set -q PYTHONHOME
		set -g _VF_OLD_PYTHONHOME $PYTHONHOME
		set -e PYTHONHOME
	end

	emit virtualenv_did_activate
	emit virtualenv_did_activate:$VIRTUAL_ENV
end
