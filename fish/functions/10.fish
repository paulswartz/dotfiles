function 10
  for i in (seq 1 10)
    echo Run $i of 10 @ (date)
    eval $argv
    if [ $status -gt 0 ]
       echo Failed!
       return $status
    end
  end
  echo Succeeded!
end