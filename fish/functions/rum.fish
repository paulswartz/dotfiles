function rum --description "Add a task to my #Matchbox queue"
    set mb_current_branch (git rev-parse --abbrev-ref HEAD 2>/dev/null)
    if not contains "$mb_current_branch" "" "master" "development"
        set mb_branch_tag "#$mb_current_branch"
    else
        set mb_branch_tag ""
    end
    ru "$argv" $mb_branch_tag "#Matchbox"
end