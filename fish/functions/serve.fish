function serve
  set path .
  if test "x$argv" != x
    set path $argv
  end

  echo "twistd -no web --path=$path"
  twistd -no web --path=$path
end