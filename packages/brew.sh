#!/bin/sh

# Installs various packages I like, using Homebrew

# If Homebrew isn't installed, then install it.
if [ ! -x /usr/local/bin/brew ]; then
    ruby -e "$(curl -fsSL https://raw.github.com/mxcl/homebrew/go)"
fi

# get ready for installing
brew update
if ! brew doctor; then exit $?; fi

# Install!
brew tap thoughtbot/formulae
brew install ack fish git tmux watch wget htop python3 terminal-notifier node ag gitsh ssh-copy-id

# Give htop root access
sudo chown root:wheel /usr/local/Cellar/htop-osx/*/bin/htop
sudo chmod u+s /usr/local/Cellar/htop-osx/*/bin/htop
