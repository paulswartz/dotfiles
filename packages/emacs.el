; Install packages I care about; borrowed from
; http://batsov.com/articles/2012/02/19/package-management-in-emacs-the-good-the-bad-and-the-ugly/
(require 'cl)
(require 'package)
(package-initialize)

(defvar paulswartz-packages
  '(coffee-mode
    color-theme
    color-theme-solarized
    flycheck
    flycheck-color-mode-line
    fill-column-indicator
    ag
    gh
    haskell-mode
    ido-ubiquitous
    json-mode
    js2-mode
    less-css-mode
    magit
    mustache-mode
    puppet-mode
    virtualenv
    yaml-mode
    window-number-mode))

(defun paulswartz-packages-installed-p ()
  (loop for p in paulswartz-packages
        when (not (package-installed-p p)) do (return nil)
        finally (return t)))

(unless (paulswartz-packages-installed-p)
  ;; check for new packages (package versions)
  (package-refresh-contents)
  ;; install the missing packages
  (dolist (p paulswartz-packages)
    (when (not (package-installed-p p))
      (package-install p))))

(provide 'paulswartz-packages)
