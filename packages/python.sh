#!/bin/sh

# Install Python-related packages

cd /tmp
curl -O https://bitbucket.org/pypa/setuptools/raw/bootstrap/ez_setup.py
sudo python ez_setup.py
curl -O https://raw.github.com/pypa/pip/master/contrib/get-pip.py
sudo python get-pip.py
rm ez_setup.py get-pip.py

pip install virtualenv flake8 httpie
