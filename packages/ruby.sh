#!/bin/sh

# Install Ruby-related packages

# RVM
curl -L https://get.rvm.io | bash -s stable
RVM="~/.rvm/bin/rvm"
$RVM requirements

# Install localtunnel for showing local servers on the web
sudo gem install localtunnel

# Install ru for adding Remember The Milk tasks from the command line
sudo gem install rumember
ru # Get the authentication token
